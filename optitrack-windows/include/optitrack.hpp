#ifndef _OPTITRACK_HPP_
#define _OPTITRACK_HPP_ 1

//#include <opencv2/core/core.hpp>
#include <stdio.h>
#include <tchar.h>
#include <conio.h>
#include <winsock2.h>

#include "NatNetTypes.h"
#include "NatNetClient.h"

#include <string>
#include <sstream>

#pragma warning( disable : 4996 )

using namespace std;


class Optitrack {

public:
	Optitrack();
	Optitrack(string localip, string serverip);
	bool init();

	void enableRigidBodyTracking(int rigidBodyId = 1); // track a rigid body?
	void disableRigidBodyTracking();
	bool isRigidBodyTrackingEnabled();
	void enableSingleMarkerTracking(); // track an individual marker -- assumes a single additional marker to be visible?
	void disableSingleMarkerTracking();
	bool isSingleMarkerTrackingEnabled();

	//bool pollNewData(); // request new data explicitly, callback seems not to work
	//bool pollNewData2();
	//

	bool newRigidBodyDataAvailable();
	bool newSingleMarkerDataAvailable();
	string getLatestRigidBodyData();
	string getLatestSingleMarkerData();

	static bool _newRigidBodyDataAvailable;
	static bool _newSingleMarkerDataAvailable;
	static string _lastRigidBodyData;
	static string _lastSingleMarkerData;
	

	static int _rigidBodyId; /// the rigidbody id we are interested in
	static bool _trackRigidBody;
	static bool _trackSingleMarker;

protected:
	
	
	
	char szMyIPAddress[128];// = "";
	char szServerIPAddress[128];// = "";
	string _localIPStr;
	string _serverIPStr;

	int analogSamplesPerMocapFrame;

	

	//bool _printVerbose; // should we printout info on the console

};
#endif