
#include <stdio.h>
#include <iostream>

#include "optitrack.hpp"

#include <string>
#include <sstream>

using namespace std;

int main(int argc, char** argv) {

	cout << "command line arguments: " << endl;
	// for now hardcode the parameters
	for (int i = 0; i < argc; i++) {
		cout << "i " << i << ": " <<  argv[i] << endl;
	
	// argv[1]: the local ip adress
    // argv[2]: the remote ip adress
	// argv[3]: the rigid body id to track
	
	// e.g., 0 127.0.0.1 127.0.0.1 2
	
	}
	if (argc == 4) {
	

		
		Optitrack ot = Optitrack(argv[1], argv[2]);

		// enable single marker and/or rigidbody tracking
		ot.enableRigidBodyTracking(stoi( string(argv[3]) ));
		//ot.enableSingleMarkerTracking();
		
		// initialize optitrack
		if (ot.init() == false) {
			cerr << "could not initialuze optitrack" << endl;
			return -1;
		}

		bool quit = false;
		
		string str3D = "na;na;na";//"px; py; pz; qx; qy; qz; qw";
		
		
		        // capture loop
		while (!quit) {
            str3D = "na;na;na";//"px; py; pz; qx; qy; qz; qw";
			

            // get 3D data
			
			if (ot.newRigidBodyDataAvailable())  {
				cout << "rb data: " << ot.getLatestRigidBodyData() << endl;
			}
			/*if (ot.newSingleMarkerDataAvailable())  {
				//cout << "sm data: " << ot.getLatestSingleMarkerData() << endl;
				str3D = ot.getLatestSingleMarkerData();
			}*/


			
			//if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
			//{
			//	cout << "esc key is pressed by user" << endl;
			//	quit = true;
			//}
		}

		
	}
	else {
		cout << "please specify the local ip adress, the remote ip adress of the optitrack server and the rigidbody id to track e.g. \"192.168.0.150 192.168.0.155 1\"";
	}
}