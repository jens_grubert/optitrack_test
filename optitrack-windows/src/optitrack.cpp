#include "optitrack.hpp"
#include <iostream>

//#define PRINT_VERBOSE 1 

bool Optitrack::_newRigidBodyDataAvailable = false;
bool Optitrack::_newSingleMarkerDataAvailable = false;
string Optitrack::_lastRigidBodyData = "";
string Optitrack::_lastSingleMarkerData = "";
int Optitrack::_rigidBodyId = 1; /// the rigidbody id we are interested in

bool Optitrack::_trackRigidBody = false;
bool Optitrack::_trackSingleMarker = false;

extern "C" {
	void __cdecl DataHandler(sFrameOfMocapData* data, void* pUserData);		// receives data from the server
	void __cdecl MessageHandler(int msgType, char* msg);		            // receives NatNet error mesages
	unsigned int MyServersDataPort = 3130;
	unsigned int MyServersCommandPort = 3131;
	NatNetClient* theClient;
	int iConnectionType = ConnectionType_Multicast;
	//int iConnectionType = ConnectionType_Unicast;
}

Optitrack::Optitrack() {
}

Optitrack::Optitrack(string localip, string serverip) {
	_localIPStr = localip;
	_serverIPStr = serverip;
	analogSamplesPerMocapFrame = 0;
	
}

bool Optitrack::init() {

	// release previous server
	/*if (theClient)
	{
		theClient->Uninitialize();
		delete theClient;
	}*/

	// create NatNet client
	theClient = new NatNetClient(iConnectionType);

	// set the callback handlers
	theClient->SetVerbosityLevel(Verbosity_Warning);
	theClient->SetMessageCallback(MessageHandler);
	theClient->SetDataCallback(DataHandler, theClient);	// this function will receive data from the server
	// [optional] use old multicast group
	//theClient->SetMulticastAddress("224.0.0.1");

	// print version info
	unsigned char ver[4];
	theClient->NatNetVersion(ver);
	printf("NatNet Sample Client (NatNet ver. %d.%d.%d.%d)\n", ver[0], ver[1], ver[2], ver[3]);

	// Init Client and connect to NatNet server
	// to use NatNet default port assignments

	strcpy(szMyIPAddress, _localIPStr.c_str());
	strcpy(szServerIPAddress, _serverIPStr.c_str());
	int retCode = theClient->Initialize(szMyIPAddress, szServerIPAddress);
	if (retCode != ErrorCode_OK)
	{
		printf("Unable to connect to server.  Error code: %d. returning false", retCode);
		return false;
	}
	else
	{
		// get # of analog samples per mocap frame of data
		void* pResult;
		int ret = 0;
		int nBytes = 0;
		ret = theClient->SendMessageAndWait("AnalogSamplesPerMocapFrame", &pResult, &nBytes);
		if (ret == ErrorCode_OK)
		{
			analogSamplesPerMocapFrame = *((int*)pResult);
			printf("Analog Samples Per Mocap Frame : %d", analogSamplesPerMocapFrame);
		}

		// print server info
		sServerDescription ServerDescription;
		memset(&ServerDescription, 0, sizeof(ServerDescription));
		theClient->GetServerDescription(&ServerDescription);
		if (!ServerDescription.HostPresent)
		{
			printf("Unable to connect to server. Host not present. Exiting.");
			return 1;
		}
		printf("[Optitrack] Server application info:\n");
		printf("Application: %s (ver. %d.%d.%d.%d)\n", ServerDescription.szHostApp, ServerDescription.HostAppVersion[0],
			ServerDescription.HostAppVersion[1], ServerDescription.HostAppVersion[2], ServerDescription.HostAppVersion[3]);
		printf("NatNet Version: %d.%d.%d.%d\n", ServerDescription.NatNetVersion[0], ServerDescription.NatNetVersion[1],
			ServerDescription.NatNetVersion[2], ServerDescription.NatNetVersion[3]);
		printf("Client IP:%s\n", szMyIPAddress);
		printf("Server IP:%s\n", szServerIPAddress);
		printf("Server Name:%s\n\n", ServerDescription.szHostComputerName);

		//////////////////////////////7

		// send/receive test request
		printf("[SampleClient] Sending Test Request\n");
		void* response;
		
		int iResult = theClient->SendMessageAndWait("TestRequest", &response, &nBytes);
		if (iResult == ErrorCode_OK)
		{
			printf("[SampleClient] Received: %s", (char*)response);
		}

		// Retrieve Data Descriptions from server
		printf("\n\n[SampleClient] Requesting Data Descriptions...");
		sDataDescriptions* pDataDefs = NULL;
		int nBodies = theClient->GetDataDescriptions(&pDataDefs);
		if (!pDataDefs)
		{
			printf("[SampleClient] Unable to retrieve Data Descriptions.");
		}
		else
		{
			printf("[SampleClient] Received %d Data Descriptions:\n", pDataDefs->nDataDescriptions);
			for (int i = 0; i < pDataDefs->nDataDescriptions; i++)
			{
				printf("Data Description # %d (type=%d)\n", i, pDataDefs->arrDataDescriptions[i].type);
				if (pDataDefs->arrDataDescriptions[i].type == Descriptor_MarkerSet)
				{
					// MarkerSet
					sMarkerSetDescription* pMS = pDataDefs->arrDataDescriptions[i].Data.MarkerSetDescription;
					printf("MarkerSet Name : %s\n", pMS->szName);
					for (int i = 0; i < pMS->nMarkers; i++)
						printf("%s\n", pMS->szMarkerNames[i]);

				}
				else if (pDataDefs->arrDataDescriptions[i].type == Descriptor_RigidBody)
				{
					// RigidBody
					sRigidBodyDescription* pRB = pDataDefs->arrDataDescriptions[i].Data.RigidBodyDescription;
					printf("RigidBody Name : %s\n", pRB->szName);
					printf("RigidBody ID : %d\n", pRB->ID);
					printf("RigidBody Parent ID : %d\n", pRB->parentID);
					printf("Parent Offset : %3.2f,%3.2f,%3.2f\n", pRB->offsetx, pRB->offsety, pRB->offsetz);
				}
			
			
				else
				{
					printf("Unknown data type.");
					// Unknown
				}
			}
		}
		/////////////////////////////////
		return true;
	}
}

//
//bool Optitrack::pollNewData2() 
//{
//	int nBytes = 0;
//	printf("[SampleClient] Sending Test Request\n");
//	void* response;
//
//	int iResult = theClient->SendMessageAndWait("TestRequest", &response, &nBytes);
//	if (iResult == ErrorCode_OK)
//	{
//		printf("[SampleClient] Received: %s", (char*)response);
//	}
//
//	// Retrieve Data Descriptions from server
//	printf("\n\n[SampleClient] Requesting Data Descriptions...");
//	sDataDescriptions* pDataDefs = NULL;
//	int nBodies = theClient->GetDataDescriptions(&pDataDefs);
//	if (!pDataDefs)
//	{
//		printf("[SampleClient] Unable to retrieve Data Descriptions.");
//	}
//	else
//	{
//		printf("[SampleClient] Received %d Data Descriptions:\n", pDataDefs->nDataDescriptions);
//		for (int i = 0; i < pDataDefs->nDataDescriptions; i++)
//		{
//			printf("Data Description # %d (type=%d)\n", i, pDataDefs->arrDataDescriptions[i].type);
//			if (pDataDefs->arrDataDescriptions[i].type == Descriptor_MarkerSet)
//			{
//				// MarkerSet
//				sMarkerSetDescription* pMS = pDataDefs->arrDataDescriptions[i].Data.MarkerSetDescription;
//				printf("MarkerSet Name : %s\n", pMS->szName);
//				for (int i = 0; i < pMS->nMarkers; i++)
//					printf("%s\n", pMS->szMarkerNames[i]);
//
//			}
//			else if (pDataDefs->arrDataDescriptions[i].type == Descriptor_RigidBody)
//			{
//				// RigidBody
//				sRigidBodyDescription* pRB = pDataDefs->arrDataDescriptions[i].Data.RigidBodyDescription;
//				printf("RigidBody Name : %s\n", pRB->szName);
//				printf("RigidBody ID : %d\n", pRB->ID);
//				printf("RigidBody Parent ID : %d\n", pRB->parentID);
//				printf("Parent Offset : %3.2f,%3.2f,%3.2f\n", pRB->offsetx, pRB->offsety, pRB->offsetz);
//			}
//
//
//			else
//			{
//				printf("Unknown data type.");
//				// Unknown
//			}
//		}
//	}
//	sFrameOfMocapData* data = NULL;
//	data = theClient->GetLastFrameOfData();
//
//	if (data == NULL) {
//		cerr << "could not receive mocap data sFrameOfMocapData" << endl;
//		return false;
//	}
//#ifdef PRINT_VERBOSE
//	printf("Rigid Bodies [Count=%d]\n", data->nRigidBodies);
//#endif
//
//	return false;
//}
//bool Optitrack::pollNewData() {
//
//	// send/receive test request
//	//printf("[SampleClient] Sending Test Request\n");
//	int nBytes = 0;
//	void* response;
//
//	_newDataAvailable = false;
//
//	int iResult = theClient->SendMessageAndWait("TestRequest", &response, &nBytes);
//	if (iResult == ErrorCode_OK)
//	{
//		printf("Received: %s", (char*)response);
//	}
//	else {
//		cerr << "no answer from server" << endl;
//	}
//
//	// Retrieve Data Descriptions from server
//	//printf("\n\n[SampleClient] Requesting Data Descriptions...");
//	/*sDataDescriptions* pDataDefs = NULL;
//	int nBodies = theClient->GetDataDescriptions(&pDataDefs);*/
//
//	sFrameOfMocapData* data = NULL;
//	data = theClient->GetLastFrameOfData();
//
//	if (data == NULL) {
//		cerr << "could not receive mocap data sFrameOfMocapData" << endl;
//		return false;
//	}
//
//	// Rigid Bodies
//#ifdef PRINT_VERBOSE
//	printf("Rigid Bodies [Count=%d]\n", data->nRigidBodies);
//#endif
//	for (int i = 0; i < data->nRigidBodies; i++)
//	{
//		// params
//		// 0x01 : bool, rigid body was successfully tracked in this frame
//		/*	bool bTrackingValid = data->RigidBodies[i].params & 0x01;*/
//
//		if (data->RigidBodies[i].ID == Optitrack::_rigidBodyId) {
//			stringstream _tmpSStream;
//			_tmpSStream << data->RigidBodies[i].x << ";" << data->RigidBodies[i].y << ";" << data->RigidBodies[i].z << ";" << data->RigidBodies[i].qx << ";" << data->RigidBodies[i].qy << ";" << data->RigidBodies[i].qz << ";" << data->RigidBodies[i].qw;
//			Optitrack::_lastData = _tmpSStream.str();
//			Optitrack::_newDataAvailable = true;
//#ifdef PRINT_VERBOSE
//			cout << "new data: " << Optitrack::_lastData << endl;
//#endif
//		}
//	} // end for
//	return _newDataAvailable;
//}

bool Optitrack::newRigidBodyDataAvailable() {
	return _newRigidBodyDataAvailable;
}

bool Optitrack::newSingleMarkerDataAvailable() {
	return _newSingleMarkerDataAvailable;
}

string Optitrack::getLatestRigidBodyData() {
	Optitrack::_newRigidBodyDataAvailable = false;
	return _lastRigidBodyData;
}

string Optitrack::getLatestSingleMarkerData() {
	Optitrack::_newSingleMarkerDataAvailable = false;
	return _lastSingleMarkerData;
}

void Optitrack::enableRigidBodyTracking(int rigidBodyId) {
	Optitrack::_rigidBodyId = rigidBodyId;
	Optitrack::_trackRigidBody = true;
}

void Optitrack::disableRigidBodyTracking() {
	Optitrack::_trackRigidBody = false;
}
bool Optitrack::isRigidBodyTrackingEnabled() {
	return Optitrack::_trackRigidBody;
}

void Optitrack::enableSingleMarkerTracking() {
	Optitrack::_trackSingleMarker = true;
}
void Optitrack::disableSingleMarkerTracking() {
	Optitrack::_trackSingleMarker = false;
}
bool Optitrack::isSingleMarkerTrackingEnabled() {
	return Optitrack::_trackSingleMarker;
}

void enableSingleMarker(); // track an individual marker?


// DataHandler receives data from the server
void __cdecl DataHandler(sFrameOfMocapData* data, void* pUserData)
{

	//cout << "DataHandler called " << endl;
	NatNetClient* pClient = (NatNetClient*)pUserData;

	//if (fp)
	//	_WriteFrame(fp, data);

	int i = 0;

#ifdef PRINT_VERBOSE
	printf("FrameID : %d\n", data->iFrame);
	printf("Timestamp :  %3.2lf\n", data->fTimestamp);
	printf("Latency :  %3.2lf\n", data->fLatency);

#endif

	//// Other Markers
	/*printf("Other Markers [Count=%d]\n", data->nOtherMarkers);
	for (i = 0; i < data->nOtherMarkers; i++)
	{
		printf("Other Marker %d : %3.2f\t%3.2f\t%3.2f\n",
			i,
			data->OtherMarkers[i][0],
			data->OtherMarkers[i][1],
			data->OtherMarkers[i][2]);
	}
*/
	if (Optitrack::_trackSingleMarker) {
		if (data->nOtherMarkers > 0) {
			// use only the first visible marker
			stringstream _tmpSStream;
			_tmpSStream << data->OtherMarkers[0][0] << ";" << data->OtherMarkers[0][1] << ";" << data->OtherMarkers[0][2];
			Optitrack::_lastSingleMarkerData = _tmpSStream.str();
			Optitrack::_newSingleMarkerDataAvailable = true;
#ifdef PRINT_VERBOSE
			cout << "new sm data: " << Optitrack::_lastSingleMarkerData << endl;
#endif
		}
	}

	// Rigid Bodies
	if(Optitrack::_trackRigidBody) {
#ifdef PRINT_VERBOSE
		printf("Rigid Bodies [Count=%d]\n", data->nRigidBodies);
#endif
		for (i = 0; i < data->nRigidBodies; i++)
		{
			// params
			// 0x01 : bool, rigid body was successfully tracked in this frame
			bool bTrackingValid = data->RigidBodies[i].params & 0x01;

			if (data->RigidBodies[i].ID == Optitrack::_rigidBodyId) {
				stringstream _tmpSStream;
				_tmpSStream << data->RigidBodies[i].x << ";" << data->RigidBodies[i].y << ";" << data->RigidBodies[i].z << ";" << data->RigidBodies[i].qx << ";" << data->RigidBodies[i].qy << ";" << data->RigidBodies[i].qz << ";" << data->RigidBodies[i].qw;
				Optitrack::_lastRigidBodyData = _tmpSStream.str();
				Optitrack::_newRigidBodyDataAvailable = true;
#ifdef PRINT_VERBOSE
				cout << "new rb data: " << Optitrack::_lastRigidBodyData << endl;
#endif
			}

#ifdef PRINT_VERBOSE

			printf("Rigid Body [ID=%d  Error=%3.2f  Valid=%d]\n", data->RigidBodies[i].ID, data->RigidBodies[i].MeanError, bTrackingValid);
			printf("\tx\ty\tz\tqx\tqy\tqz\tqw\n");
			printf("\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\n",
				data->RigidBodies[i].x,
				data->RigidBodies[i].y,
				data->RigidBodies[i].z,
				data->RigidBodies[i].qx,
				data->RigidBodies[i].qy,
				data->RigidBodies[i].qz,
				data->RigidBodies[i].qw);

			printf("\tRigid body markers [Count=%d]\n", data->RigidBodies[i].nMarkers);
			for (int iMarker = 0; iMarker < data->RigidBodies[i].nMarkers; iMarker++)
			{
				printf("\t\t");
				if (data->RigidBodies[i].MarkerIDs)
					printf("MarkerID:%d", data->RigidBodies[i].MarkerIDs[iMarker]);
				if (data->RigidBodies[i].MarkerSizes)
					printf("\tMarkerSize:%3.2f", data->RigidBodies[i].MarkerSizes[iMarker]);
				if (data->RigidBodies[i].Markers)
					printf("\tMarkerPos:%3.2f,%3.2f,%3.2f\n",
					data->RigidBodies[i].Markers[iMarker][0],
					data->RigidBodies[i].Markers[iMarker][1],
					data->RigidBodies[i].Markers[iMarker][2]);
			}
#endif
		}
	} // endif track rigidbody
}

// MessageHandler receives NatNet error/debug messages
void __cdecl MessageHandler(int msgType, char* msg)
{
	printf("\n%s\n", msg);
}